#!/bin/bash

rm *.aux
rm *.toc
rm *.pdf
rm *.lot
rm *.log
rm *.idx
rm *.blg
rm *.bbl
rm *.gz
rm *.lof
rm *.out

