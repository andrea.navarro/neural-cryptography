## Intrucciones de instalación

### Instalar Conda

	https://docs.conda.io/projects/conda/en/latest/user-guide/install/

### Instalar Pytorch

	Con el entorno activado instalar pytorch según las instrucciones
	
	https://pytorch.org/
	
### Ejecutar el programa

	python3 test.py -f ./tests/nombre_archivo.csv

Este comando tomará como entrada el archivo nombre_archivo.csv y realizará las pruebas para todas las líneas del mismo.
Como resultado creará un archivo dentro de ./restults/ por cada línea del csv.
