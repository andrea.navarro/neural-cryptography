import csv
import gc
import getopt
import sys
from concurrent.futures import ProcessPoolExecutor, wait, as_completed

from tpm import TPM

(opt, arg) = getopt.getopt(sys.argv[1:], 'l:n:k:e:r:t:f:i:j:p:')

tests = 1000
path = "./results/"

F = False


def test_function(l, n, k, rule, epoch):
    l = int(l)
    n = int(n)
    k = int(k)
    epoch = int(epoch)

    filename = str(l) + "-" + str(n) + "-" + str(k) + "-" + str(epoch) + "-" + rule
    with open(path + filename + ".csv", 'w', newline='') as file:
        writer = csv.writer(file)
        for i in range(1, tests):
            nn = TPM(l, n, k, epoch, rule)
            print("Test Nº " + str(i))
            s = nn.synchronization()
            if s:
                print("Convergencia")
            writer.writerow([l, n, k, epoch, rule, s])

            del nn, s
            gc.collect()

        del writer
        gc.collect()

for (op, ar) in opt:
    if (op in ['-f']):
        f = ar
        F = True
    elif (op in ['-i']):
        i = int(ar)
    elif (op in ['-j']):
        j = int(ar)
    elif (op in ['-l']):
        l = int(ar)
    elif (op == '-n'):
        n = int(ar)
    elif (op == '-k'):
        k = int(ar)
    elif (op == '-e'):
        epoch = int(ar)
    elif (op == "-r"):
        rule = ar
    elif (op == "-t"):
        tests = int(ar)
    elif (op == "-p"):
        procs = int(ar)
    else:
        raise Exception("Invalid option")

if F:
    pool = ProcessPoolExecutor(max_workers = procs)
    #pool = ProcessPoolExecutor(max_workers = 4)
    with open(f, "r") as reader:
        for line in reader:
            print(line)
            l, n, k, rule, epoch = line.split(",")
#            test_function(l, n, k, rule, epoch)
            future = pool.submit(test_function, l, n, k, rule, epoch)


else:
    test_function(l, n, k, rule, epoch)

# py test.py -f ./tests/registro_giancarlo.csv -p 3

"""
Tomando como base una clave de 256 bits que corresponden a 64 dígitos hexadecimales se crea una TPM con 32 conexiones(pesos)
cuyos pesos tienen un rango de 8 bits(2 caracteres hexa).
L es seteada como 128 lo que generará pesos entre los valores de (-128,127)
Cuando se genera la clave a estos valores se les suma 128 de manera que se encuentren en el rango 0-255
Para generar las 32 conexiones existen 4 estructuras posibles:
n = 2  k = 16
n = 16 k = 2
n = 8  k = 4
n = 4  k = 8
"""
