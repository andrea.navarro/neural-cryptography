import getopt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import random
import sys
import torch
from matplotlib import animation

torch.set_printoptions(profile="full")


# from test import l,n,k,epoch, rule

## TODO:
# Automatizar para evaluar el tiempo de sincronizacion para:
# Traducir pesos a claves binarias
# Parametrizar algoritmo de aprendizaje
# Modifcar la clave para 256
# Optimizacion

class TPM:
    """
    n= Cantidad de neuronas de entrada por cada cada oculta
    k= Cantidad de neuronas ocultas
    epoch= Cantidad de iteraciones
    rule = Regla de aprendizaje
    l= Rango de los pesos
    """

    l = 10
    n = 10
    k = 10
    epoch = 100
    rule = "antihebb"

    # parameterized constructor
    def __init__(self, l, n, k, epoch, rule):
        self.l = l
        self.n = n
        self.k = k
        self.epoch = epoch
        self.rule = rule

    # Funcion para llenado inicial de pesos
    def weights_init(self, m):
        # Verificar que sea una capa Lineal
        if isinstance(m, torch.nn.Linear):
            # Crear distribucion uniforme de pesos entre -l y l
            torch.nn.init.uniform_(m.weight, -self.l, (self.l - 1))
            # Pasar pesos a entero
            m.weight = torch.nn.Parameter(m.weight.round())

    # Imprimir pesos
    def show_weights(self, m):
        print("PESOS:")
        for subnet in m.subnets:
            # print(str(subnet.output.weight))
            print(subnet.output.weight.data.cpu().numpy())

    # Mostrar clave en formato hex
    def show_key(self, m):
        key = ''
        for subnet in m.subnets:
            weights = subnet.output.weight.data.cpu().numpy()
            for i in weights:
                for j in i:
                    # Convertir cada peso en un valor decimal entre 0 y (l*2)-1 (l*2 valores en total)
                    # Convertir valor decimal a hexadecimal 2 dígitos hexa por cada dígito decimal
                    # Concatenar como parte de la clave
                    key += str(hex(int(j) + self.l + 1))[2:]
        # print("Clave:"+str(key))
        return key

    # Modificar pesos
    def update_weights(self, m, inputs, output, rule="hebb"):
        i = 0
        for subnet in m.subnets:
            # Modificar solamente donde la salida de la neurona oculta coincida con la salida de la red
            if subnet.sout == output:
                weights = {}
                for j in range(self.n):
                    # Mofidificación de pesos por regla de HEBB
                    if self.rule == "hebb":
                        n_weight = subnet.output.weight[:, j] + (output * inputs[i][j])
                    elif self.rule == "antihebb":
                        n_weight = subnet.output.weight[:, j] - (output * inputs[i][j])
                    elif self.rule == "random_walk":
                        n_weight = subnet.output.weight[:, j] + inputs[i][j]
                    else:
                        raise Exception("Learning rule not valid")
                    # Modificar salida si se sale del rango de pesos
                    if abs(n_weight) > self.l:
                        n_weight = torch.sign(n_weight) * self.l
                    subnet.output.weight[:, j] = n_weight
            i += 1

    def synchronization(self):

        with torch.no_grad():
            # Inicializar el modelo
            modelA = Net(self.n, self.k)
            modelB = Net(self.n, self.k)
            # Inicializar pesos de los submodelos
            for subnet in modelA.subnets:
                subnet.apply(self.weights_init)
            # Inicializar pesos de los submodelos
            for subnet in modelB.subnets:
                # subnet.weight.detach()
                subnet.apply(self.weights_init)

            for step in range(self.epoch):
                # Prueba con valores de entrada para los submodelos
                inputs = []
                for i in range(self.k):
                    randInputs = []
                    for j in range(self.n):
                        randInputs.append([-1, 1][random.randrange(2)])
                    inputs.append(torch.tensor(randInputs, requires_grad=False).float())

                outputA = modelA.forward(inputs)
                outputB = modelB.forward(inputs)
                if outputA == outputB:
                    self.update_weights(modelA, inputs, outputA, self.rule)
                    self.update_weights(modelB, inputs, outputA, self.rule)
            # self.show_weights(modelA)
            # self.show_weights(modelB)
            keyA = self.show_key(modelA)
            keyB = self.show_key(modelB)
            if keyA == keyB:
                return 1
            else:
                return 0


# Sub Red que representa un neurona oculta conectada a las neuronas de entrada correspondientes
class SubNet(torch.nn.Module):
    def __init__(self, n=2):
        super(type(self), self).__init__()
        # Conexion lineal entre capa de entrada y oculta
        self.output = torch.nn.Linear(n, 1, bias=False)
        # Función de activacion de signo
        self.activation = torch.sign

    def forward(self, x):
        # Calculo de la salida de la subred
        out = self.activation(self.output(x))
        # Modificar el valor de salida para que sea -1 cuando la salida es 0
        out[out == 0] = -1
        self.sout = out
        return out


# Red que incluye todas las subredes y neurona de salida
class Net(torch.nn.Module):
    def __init__(self, n=2, k=2):
        super(type(self), self).__init__()
        self.subnets = []
        for i in range(k):
            self.subnets.append(SubNet(n))
        self.output = torch.nn.Linear(k, 1)

    def forward(self, inputs):
        output = 1
        i = 0
        for subnet in self.subnets:
            output *= subnet(inputs[i])
            i += 1
        return output


"""
Cuando las salidas de las redes sean la misma:
HEBB
Nuevo peso: Viejo peso + (salida de la red * la entrada a la neurona de entrada)
wkj+ = wkj + (O(a/b) * Xkj )

ANTI HEBB

Nuevo peso: Viejo peso - (salida de la red * la entrada a la neurona de entrada)
wkj+ = wkj - (O(a/b) * Xkj )

RANDOM WALK
Nuevo peso: Viejo peso + la entrada a la neurona de entrada)
wkj+ = wkj + (Xkj )

Si un peso se sale de rango -L L
Wkj = sign(wk)*L

"""
