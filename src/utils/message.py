
"""
Ejemplo hello cliente
    helloC      01
    v           01
    n           0014 (20)
    k           01
    epoch       00c8 (200)
    regla       02
    l           08
    clave       00
    salida      01
    rand        0001000100010000010101000100010001010001

010100140100c8020800010001000100010000010101000100010001010001


"""



def l_to_bit(i):
    if i == -1:
        return 0
    else:
        return 1

def bit_to_l(i):
    if i == 0:
        return -1
    else:
        return 1

"""
Recibe el mensaje en bits y devuelve los parámetros
"""
def encode_frame(**kwargs):
    frame = ""

    if "hello" in kwargs:
        if kwargs["hello"] == 'c':
            frame += "10"
            frame += f"{kwargs['v']:02x}"
            frame += f"{kwargs['n']:04x}"
            frame += f"{kwargs['k']:02x}"
            frame += f"{kwargs['epoch']:04x}"
            if kwargs["rule"] == "hebb":
                frame += "01"
            elif kwargs["rule"] == "antihebb":
                frame += "02"
            else:
                frame += "03"
            frame += f"{kwargs['l']:02x}"
            frame += "00"
        else:
            frame += "20"
            frame += "00"


    if "out_response" in kwargs:
        frame += f"{l_to_bit(kwargs['out_response']):02x}"
    
    frame += f"{l_to_bit(kwargs['out']):02x}"

    for r in kwargs['rand']:
        frame += f"{l_to_bit(r):02x}"


    print(frame)
    return frame


def decode_frame(frame):
    params = {}
    i=0
    if frame[i] == '1': # hello cliente
        i+=2
        params['v'] = int(frame[i:i+2])
        i+=2
        params['n'] = int(frame[i:i+4],16)
        i+=4
        params['k'] = int(frame[i:i+2])
        i+=2
        params['epoch'] = int(frame[i:i+4],16)
        i+=4
        rule = int(frame[i:i+2],16)
        if rule == 1:
            params['rule'] = "hebb"
        elif rule == 2:
            params['rule'] = "antihebb"
        else:
            params['rule'] = "randomwalk"
        i+=2
        params['l'] = int(frame[i:i+2])
        i+=2
        params['key'] = int(frame[i:i+2],16)
        i+=2

        params['out'] = bit_to_l(int(frame[i:i+2],16))
        i+=2
        rand = frame[i:]
        params['rand'] = [bit_to_l(int(frame[i:][j:j+2],16)) for j in range(0, len(frame[i:]), 2)]

    elif frame[i] == '2': # hello servidor
        i+=2
        params['ack'] = int(frame[i:i+2])
        i+=2

        params['out_response'] = bit_to_l(int(frame[i:i+2],16))
        i+=2
        params['out'] = bit_to_l(int(frame[i:i+2],16))
        i+=2
        rand = frame[i:]
        params['rand'] = [bit_to_l(int(frame[i:][j:j+2],16)) for j in range(0, len(frame[i:]), 2)]

    else:
        params['out_response'] = bit_to_l(int(frame[i:i+2],16))
        i+=2
        params['out'] = bit_to_l(int(frame[i:i+2],16))
        i+=2
        rand = frame[i:]
        params['rand'] = [bit_to_l(int(frame[i:][j:j+2],16)) for j in range(0, len(frame[i:]), 2)]

    return params





    print(params)

if __name__ == "__main__":

    helloc = encode_frame(hello='c', v=1, n=20, k=1, epoch=200, rule="hebb", l=8, key=0, out=-1, rand=(1,-1,-1,1,1,-1,-1,-1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1))
    ret = decode_frame(helloc)
    print(ret)

    hellos = encode_frame(hello='s', out=1, out_response=-1, rand=(1,-1,-1,1,1,-1,-1,-1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1))
    ret = decode_frame(hellos)
    print(ret)

    msg = encode_frame(out=1, out_response=-1, rand=(1,-1,-1,1,1,-1,-1,-1,1,1,-1,-1,1,-1,1,-1,-1,1,1,1))

    ret = decode_frame(msg)
    print(ret)






