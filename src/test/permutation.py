import csv

"""structures = [
			{"n":2,"k":16},
			{"n":18,"k":2},
			{"n":4,"k":8},
			{"n":8,"k":4}
			]"""

entradas = [32]

ocultas = [1]

rules = ["hebb"]

epochs = range(350, 600, 50)

pesos = [8, 16, 32]
l = 8

tests = []

with open("registro-andrea-5.csv", 'w', newline='') as file:
    writer = csv.writer(file)
    for e in epochs:
        for n in entradas:
            for k in ocultas:
                for r in rules:
                    writer.writerow([l, n, k, r, e])
