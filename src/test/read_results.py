import csv
import os

# Read folders
dataLocation = "results"
results = [x[2] for x in os.walk(dataLocation)]
print(results)
for result in results:
    results_file_name = "summary.csv"
    with open(results_file_name, 'w', newline='') as file:
        writer = csv.writer(file)
        for fileName in result:
            if fileName != "README.md":
                print(dataLocation + "/" + fileName)
                with open(dataLocation + "/" + fileName, "r") as f:
                    # Read line
                    convergencia = []
                    for line in f:
                        # Split line
                        inner_list = [elt.strip() for elt in line.split(',')]
                        convergencia.append(int(inner_list[5]))
                    l = inner_list[0]
                    n = inner_list[1]
                    k = inner_list[2]
                    epoch = inner_list[3]
                    rule = inner_list[4]
                    porcentaje = round(sum(convergencia) / 1000 * 100, 2)
                    print(porcentaje)
                    writer.writerow([l, n, k, epoch, rule, porcentaje])
