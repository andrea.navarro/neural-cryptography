% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.21 of 2022/01/12
%
\documentclass[runningheads]{llncs}
%
\usepackage[T1]{fontenc}
% T1 fonts will be used to generate the final print and online PDFs,
% so please use T1 fonts in your manuscript whenever possible.
% Other font encondings may result in incorrect characters.
%
\usepackage{graphicx}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following two lines
% to display URLs in blue roman font according to Springer's eBook style:
%\usepackage{color}
%\renewcommand\UrlFont{\color{blue}\rmfamily}
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% traduccion al español
\usepackage[spanish, es-tabla, activeacute]{babel}
% comentarios
\usepackage{comment}

% secciones de código
\usepackage{listings}
\renewcommand{\lstlistingname}{Algoritmo}
\renewcommand{\lstlistlistingname}{Índice de algoritmos}

% colores en los códigos
\usepackage{color}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

% para comillas con \q y \qq
\usepackage{textcmds}

% para marcas TODO
\usepackage{todonotes}

% añadir el import{Formatos} en el documento
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

\begin{document}
%


\title{Protocolo de intercambio de claves basado en sincronización de Redes Neuronales Artificiales}
%
\titlerunning{Intercambio de claves basado en redes neuronales}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Diego Córdoba
\and
Andrea Navarro
\and
Mariena Asensio
\and
Esteban Rivas Vizcaino
\and
Gianfranco Galvarini
\and
Pablo Fracaro
}
%
\authorrunning{D. Córdoba et al.}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{Universidad de Mendoza, Facultad de Ingeniería, San Rafael, Mendoza, Argentina \\
\email{\{diego.cordoba, andrea.navarro, mariela.asensio\}@um.edu.ar}\\
\url{https://www.um.edu.ar}}
%

\maketitle              % typeset the header of the contribution
%
\begin{abstract}
Gran parte de las comunicaciones en Internet utilizan algoritmos criptográficos para garantizar la seguridad de sus mensajes. Estos algoritmos requieren, en general, de un intercambio previo de sus claves. Diffie-Hellman (DH) es uno de los protocolos de intercambio de claves más utilizado, y permite a dos extremos obtener la misma clave simétrica sin revelar información confidencial a un posible atacante. DH se basa en el intercambio de números enteros públicos y operaciones de los mismos con números privados en cada extremo, de modo que ambos puedan calcular una misma cifra sin haberla compartido en el canal inseguro. Un atacante que pueda obtener los números públicos intercambiados no podría calcular la clave secreta en un tiempo acotado sin disponer de los números privados. Este protocolo de intercambio de claves se basa en principios de complejidad computacional en los que se hace uso de operaciones matemáticas sencillas de calcular, pero complejas de revertir con las capacidades de procesamiento actuales. Este tipo de protocolos se consideran vulnerables al criptoanálisis realizado desde computadoras cuánticas que cuenten con la suficiente cantidad de qubits y una gestión aceptable del ruido cuántico.

Uno de los mecanismos de intercambio de claves considerados seguros ante el criptoanálisis cuántico es el basado en sincronizar redes neuronales artificiales. El presente trabajo da cuenta de los experimentos realizados para seleccionar la arquitectura de red neuronal adecuada, el posterior proceso de implementación en lenguaje Python, y la realización de pruebas utilizando PyTorch como marco de trabajo de gestión de redes neuronales.

\keywords{criptografía  \and redes neuronales \and tree parity machine \and intercambio de claves}.
\end{abstract}
%
%
\input{Formatos}
%
\section{Introducción}

La mayoría de los mecanismos criptográficos utilizados en las comunicaciones en Internet hacen uso de protocolos que brindan seguridad a los mensajes transmitidos, en términos de confidencialidad, autenticidad e integridad. Estos mecanismos utilizan algoritmos criptográficos simétricos y asimétricos para lograr su objetivo \cite{lucenalopezCriptografiaSeguridadComputadores2011}. Los algoritmos simétricos hacen uso de una única clave, que debe ser pre-compartida entre los extremos de la comunicación. El esquema de criptosistema de un algoritmo simétrico puede verse en la Fig. \ref{fig:cryptosystem}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/cryptosystem.png}
	\caption{\textbf{Criptosistema simétrico.}}
	\label{fig:cryptosystem}
\end{figure}

Tanto emisor como receptor deben compartir la misma clave para poder intercambiar información privada. El emisor del mensaje hará uso de la clave simétrica y de un algoritmo criptográfico para transformar un mensaje original, llamado mensaje en texto plano, en un mensaje cifrado. Este mensaje cifrado únicamente podrá ser descifrado por el destinatario, que hará uso de un algoritmo de descifrado y de la clave simétrica para volver a obtener el mensaje original.

Por lo enunciado, es requisito que ambos extremos de una comunicación, previo a intercambiar mensajes cifrados, posean la misma clave de cifrado simétrico. Claro está que no pueden enviarla en texto plano por medio del mismo canal de comunicación, ya que un atacante podría capturarla. Aquí se hace uso de algoritmos de intercambio de claves tales como Diffie-Hellman (DH) \cite{rescorla_rfc_1999}, que permiten a dos extremos obtener la misma clave de cifrado sin haberla enviado públicamente por el canal de comunicación. Particularmente DH se basa en el intercambio de números enteros públicos y operaciones de los mismos con números privados en cada extremo. Esto permite a ambos extremos calcular una misma cifra o clave sin haberla compartido en el canal inseguro. El atacante, en este caso, no podrá calcular dicha clave debido a falta de información. Esta clave generada se basa en cálculos matemáticos unidireccionales, simples de resolver en un sentido, pero muy difíciles de revertir con las tecnologías de procesamiento actuales.

Al igual que Diffie-Hellman, un protocolo de intercambio de claves basado en la sincronización de redes neuronales artificiales permitiría a dos extremos en una comunicación intercambiar una clave simétrica sin enviarla por el canal público \cite{stypinskiSynchronizationTreeParity2021,javurekUseArtificialNeural}.

El presente trabajo muestra el diseño e implementación de un protocolo de intercambio de claves basado en la sincronización de redes neuronales Tree Parity Machine en Python utilizando la librería Pytorch \cite{pytorch_pytorch}.

\subsection{Redes Neuronales Artificiales}

Las redes neuronales artificiales son modelos matemáticos que emulan el funcionamiento de las redes neuronales biológicas \cite{yegnanarayana2009artificial}. Consisten en un conjunto de unidades de procesamiento (neuronas), conectadas entre si por enlaces ponderados (conexiones), formando de esta manera diferentes arquitecturas. Cada neurona recibe, por medio de sus conexiones o enlaces, la información desde el exterior, la procesa y devuelve, como resultado, un valor de salida. Las neuronas de entrada recibirán, entonces, valores desde el exterior de la red, y sus resultados serán enviados hacia otras neuronas intermedias. Los resultados se encadenarán hasta alcanzar la neurona de salida, que retornará el resultado de la red completa. Cada conexión posee un peso que permitirá ponderar el valor que transita por dicho enlace de neurona a neurona. El conjunto de las neuronas de entrada se denomina \qq{capa de entrada}, mientras que las neuronas de salida se agrupan en la denominada \qq{capa de salida}. Las neuronas intermedias pertenecen a \qq{capas ocultas}.

Las redes pueden ser entrenadas para aprender a identificar diferentes patrones de entrada mediante diversos algoritmos o reglas de aprendizaje \cite{javurekUseArtificialNeural}, que se encargan de modificar gradualmente los pesos de las conexiones, adaptando así la configuración de la red. Los algoritmos de aprendizajes utilizados en el presente trabajo fueron tres:

\begin{itemize}
	\item Regla de Hebb
	\item Regla de Anti Hebb
	\item Regla de Random Walk
\end{itemize}

\subsection{Tree parity machine}

Las redes Tree Parity Machine son un subtipo de redes neuronales feed-forward que cuentan con una arquitectura específica \cite{padilla}. La capa de salida consiste en una única neurona. La capa intermedia u oculta contiene un número $K$ de neuronas, y la capa de entrada contienen un número $k*n$ de neuronas. Las capas oculta y de entrada no están completamente conectadas, sino que cada grupo de $n$ neuronas de entrada se conecta con una y sólo una neurona de la capa oculta. A su vez, cada neurona de la capa oculta se conecta únicamente con la neurona de salida. Esta arquitectura puede verse en la Fig. \ref{fig:tpm}. Aquí se pueden apreciar las neuronas de entrada en la parte inferior de la imagen, y la de salida en la parte superior. En este caso particular de red neuronal los valores de entrada (neuronas de la capa de entrada) pueden ser $1$ o $-1$.

\begin{equation}\label{key}
	X_{ij}  \in  \{-1,1\}
\end{equation}

Los pesos de las conexiones entre la capa de entrada y la de salida toman valores enteros entre $-L$ y $L$.

\begin{equation}\label{key}
	X_{ij} \in  \{-L,L\}
\end{equation}

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.7\textwidth]{img/tpm.png}
	\caption{Arquitectura Tree Parity Machine.}
	\label{fig:tpm}
\end{figure}

La salida de las neuronas de la capa oculta se calculan como la función sigmoidal de la sumatoria de la multiplicación entre la entrada de cada neurona y el peso de la conexión correspondiente. Está salida tomará valores ${-1,0,1}$. Particularmente en una TPM si el resultado es $0$ la salida se modificará para tomar un valor $-1$ garantizando de esta manera que la salida de cada neurona sea siempre $1$ o $-1$ (salida binaria). La Ecuación \ref{eq:sigmoide} muestra este cálculo.

\begin{equation}\label{eq:sigmoide}
\sigma_{i} = sgn(\sum_{j=i}^{n} w_{ij}x_{ij})
\end{equation}

Finalmente la salida de la neurona de la capa final, es decir, la salida de la red, es calculada como la multiplicaciones de las salidas de todas las neuronas de la capa oculta.

\subsection{Sincronización de TPM}

Si se tienen dos nodos que necesitan realizar una comunicación, y ambos disponen de una red TPM con la misma arquitectura (cantidad de neuronas, capas y conexiones), ambas inicializadas con pesos aleatorios en sus conexiones, se dice que se sincronizan cuando, luego de realizar una serie de aprendizajes mutuos, ambas redes obtienen los mismos pesos en sus enlaces.

Para cada paso del proceso de sincronización ambas partes ingresaran es sus redes una serie de números aleatorios como entrada. La salidas generadas por la red serán intercambiadas y comparadas entre las dos partes. Si las salidas de ambas redes son diferentes entonces no se realizará ninguna modificación en la configuración de pesos. Sin embargo, si las salidas son iguales se modificarán los pesos siguiendo una de las siguientes reglas de aprendizaje, donde $w_{ij}$ representa el peso del enlace, $o$ la salida de la red, y $x_{ij}$ representa las entradas.

\begin{itemize}
\item Hebb: $w_{ij} = w_{ij} + ( o *  x_{ij})$
\item Anti Hebb: $w_{ij} = w_{ij} - ( o *  x_{ij})$
\item Random Walk: $w_{ij} = w_{ij} + x_{ij}$
\end{itemize}

Este proceso se repetirá hasta que ambas redes tegan los mismo valores de pesos. Estos valores luego permitirán generar la clave secreta de cifrado simétrico.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth]{img/algoritmo_sincronización_tpm.png}
	\caption{Algoritmo de sincronización de Tree Parity Machine.}
	\label{fig:algoritmo_sincronización_tpm}
\end{figure}


\subsection{Pytorch}

PyTorch \cite{pytorch_pytorch} es una librería de código que brinda soporte a la programación de redes neuronales en Python. Posee dos características importantes. Por un lado implementa una estructura de datos multidimensional llamada tensor, destinada a almacenar una colección de números. Los tensores pueden ejecutar sus operaciones en una GPU, y de esta forma permiten evaluar los gradientes, esenciales para los algoritmos de aprendizaje de las redes neuronales. Por otro lado Pytorch ofrece diferenciación automática, lo que les permite realizar la propagación hacia atrás (back propagation) en el entrenamiento de las redes neuronales.

Los tensores son las estructuras de datos fundamentales de PyTorch, y pueden interpretarse como una matriz, es decir, una estructura de datos que almacena una colección de números accesibles a través de índices. Un tensor unidimensional se denomina vector y uno bidimencinoal matriz. Tensores de más dimensiones se denominan simplemente tensor.

PyTorch no es la única librería que se ocupa de las matrices multidimensionales. NumPy es la librería de matrices multidimensionales más popular. Comparando PyTorch con NumPy, los tensores de PyTorch presentan algunas ventajas tales como la capacidad de realizar operaciones en GPUs, la distribución de operaciones en múltiples dispositivos, y el seguimiento de la gráfica de cálculos que los generaron al tensor.

PyTorch está compuesto por diferentes módulos y clases con funcionalidades específicas. Por ejemplo, \verb|torch.nn| se utiliza para la creación y entrenamiento de redes neuronales, y \verb|torch.optim| para los algoritmos de optimización utilizados en el entrenamiento de las redes neuronales. Dentro de \verb|torch| también se encuentra la clase \verb|torch.Tensor|, que agrupa todas sus características.

Los tensores poseen tres propiedades importantes:

\begin{itemize}
	\item \verb|Rank|: número de dimensiones del tensor.
	\item \verb|Shape|: número de elementos que tiene en cada dimensión.
	\item \verb|Type|: tipo de dato asignado a los elemento del tensor
\end{itemize}

Existen diferentes modos de crear un tensor en PyTorch: 

\begin{itemize}
	\item Llamar un constructor del tipo requerido.
	\item  Pedirle a la librería de PyTorch que cree un tensor pasándole los datos específicos. Esta opción fue la utilizada en el presente trabajo.
\end{itemize}

\section{Desarrollo}	

A continuación se describirá el protocolo de intercambio de claves propuesto, la forma de creación de la red TPM, y el modo en el que se genera la clave a partir de los pesos de los enlaces luego de finalizado el proceso de sincronización.
	
\subsection{Diseño de protocolo}
		
El programa realizará una tarea de sincronización entre dos partes (Alice y Bob). Estas dos partes funcionarán como servidor y cliente respectivamente. El servidor se quedará esperando una comunicación en un puerto TCP determinado, y el cliente será el encargado de iniciar la conexión. Esta conexión permitirá a ambos nodos negociar los parámetros iniciales de la red TPM, y commenzar el proceso de sincronización. 

El mensaje inicial enviado por el cliente deberá incluir un saludo y las especificaciones necesarias para la creación de la red neuronal y el funcionamiento del protocolo. Estos parámetros se detallan a continuación.

\begin{itemize}
\item $v$: versión del protocolo (01 inicialmente).
\item $n$: cantidad de neuronas de entrada.
\item $k$: cantidad de neuronas en la capa oculta.
\item $e$: cantidad de iteraciones antes de detener la sincronización.
\item $r$: regla de aprendizaje utilizada (hebb, antihebb, randomwalk).
\item $l$: rango en el valor de los pesos.
\item $g$: función de generación de clave en base a los pesos. Default en $0$, para uso futuro.
\end{itemize}

Junto a este mensaje se enviará el primer valor de entrada aleatoria, $R_1$, y la primer salida de la red para ese valor, $OC_1$.

Los pasos definidos en el protocolo durante todo el intercambio entre cliente y servidor se enumeran a continuación:

\begin{enumerate}
	\item El cliente crea su red TPM seleccionando sus valores $v$, $n$, $k$, $e$, $r$ y $l$.
	\item El cliente genera el número random de entrada, $R_1$.
	\item El cliente ingresa $R_1$ en la red y obtiene la salida $OC_1$.
	\item El cliente envía al servidor el primer mensaje al servidor, \verb|HelloC|, con los parámetros para la creación de la red, junto con $OC_1$ y $R_1$.
	\item El servidor crea su red TPM en base a los parámetros enviados por el cliente.
	\item El servidor ingresa $R_1$ en la red y obtiene su salida, $OS_1$.
	\item El servidor compara $OS_1$ con $OC_1$ y actualiza los pesos de sus enlaces.
	\item El servidor genera la segunda entrada aleatoria, $R_2$.
	\item El servidor ingresa $R_2$ en la red y obtiene la salida, $OS_2$.
	\item El servidor envía al cliente $OS_1$, $OS_2$ y $R_2$. 
	\item El cliente compara su salida $OC_1$ con la salida del servidor para la misma entrada, $OS_1$, y actualiza los pesos de sus enlaces.
	\item El cliente ingresa la segunda entrada, $R_2$, en la red, y obtiene la salida $OC_2$.
	\item El cliente compara su salida $OC_2$ con la salida del servidor para la misma entrada, $OS_2$, y actualiza los pesos.
	\item El cliente genera la tercer entrada aleatoria, $R_3$.
	\item El cliente ingresa $R_3$ en su red y obtiene la salida $OC_3$
	\item El cliente envía al servidor $OC_2$, $OC_3$ y $R_3$.
	\item Se repiten los pasos 6-16 con los nuevos valores generados, hasta que se complete la cantidad de iteraciones (\textit{epochs} o $e$) establecidas por el cliente en su primer mensaje.
\end{enumerate}	

La sincronización se logrará si ambos nodos, cliente y servidor, obtienen los mismos pesos en sus enlaces. Para lograr esto se propone la siguiente secuencia:

\begin{enumerate}
	\item Ambos nodos generarán una clave simétrica de cifrado en base a los pesos de los enlaces de su red TPM. La función de generación de clave aún no ha sido definida.
	\item Ambos nodos calcularán un HASH de su clave.
	\item El cliente encriptará dicho hash haciendo uso de la clave simétrica definida en el punto 1.
	\item El cliente enviará el mensaje cifrado al servidor.
	\item El servidor descifrará el mensaje recibido utilizando su clave simétrica generada en el punto 1.
	\item El servidor comparará el hash recibido con el hash de su clave simétrica. El servidor generará un mensaje de respuesta, afirmativo o negativo, de acuerdo al resultado de su comparación.
	\item El servidor enviará dicho mensaje al cliente.
\end{enumerate}

El diagrama de secuencia de todo el intercambio, incluida la verificación de sincronismo, puede verse en la Fig. \ref{fig:secuencia}. $OS_e$ representa la última salida (para el último \textit{epoch}).

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.1\textwidth]{img/secuencia.png}
	\caption{Protocolo.}
	\label{fig:secuencia}
\end{figure}
	
\subsection{Creación de TPM con Pytorch} 

La redes neuronales feed-fordward por defecto tienen una arquitectura con capas completamente conectadas, es decir, todas las neuronas de una capa tienen una conexión con todas las neuronas de la capa siguiente. Las TPM, sin embargo, tienen una arquitectura diferente, donde cada neurona de la capa oculta tiene conexión solamente con un subgrupo de las neuronas de capa de entrada. 
De manera similar, la forma en la que se calcula la salida de cada neurona de la capa oculta difiere de la formula de suma ponderadas de entradas utilizada en las redes feed-forward tradicionales.

Para implementar la arquitectura de las redes TPM en PyTorch se creó una red neuronal personalizada, en la que cada conjunto de neuronas de entrada que se conectan a una neurona de la capa oculta se implementan como redes feed-forward individuales. Estas redes individuales, denominadas subredes (\verb|SubNet| en el código) luego se integran formando la red neuronal TPM principal, con forma de árbol, que las conectará con una única neurona de salida, requisito de la arquitectura TPM.

El siguiente segmento de código representa la clase \verb|SubNet|, una red neuronal feed-forward con una determinada cantidad de neuronas de entrada ($n$) y una neurona de salida.

\begin{lstlisting}[
language={Python},
caption={},
label={alg:subnet},
%firstnumber=1,
numbers=none,
]
class SubNet(torch.nn.Module):
	def __init__(self, n=2):
		super(type(self), self).__init__()
		# Conexion entre capa de entrada y neurona oculta
		self.output = torch.nn.Linear(n, 1, bias=False)
		# Funcion de activacion de signo
		self.activation = torch.sign
	
	def forward(self, x):
		# Calculo de la salida de la subred
		out = self.activation(self.output(x))
		# Fuerza que la salida sea -1 cuando es 0 (binario)
		out[out == 0] = -1
		self.sout = out
		return out
\end{lstlisting}

Cada \verb|SubNet|, que representa una neurona oculta con sus respectivas neuronas de entrada, se define como una red neuronal con una conexión lineal a cada neurona de entrada, y una función de activación de signo debido a que la salida de cada neurona oculta debe tomar los valores $[-1,1]$. La función de salida de esta SubNet, que representa la salida de la neurona oculta, es modificada para convertir los valores $[0,1]$ a los valores $[-1,1]$ esperados por la neurona de salida.

El siguiente fragmento representa la clase \verb|Net|, la red TPM, formada como un conjunto de subredes \verb|SubNet|. La red TPM se define como una red neuronal que une cada \verb|SubNet| con una neurona de salida mediante una conexión lineal. La función de salida de esta neurona se calcula como la multiplicación de las salidas de cada \verb|SubNet| individual. La cantidad de \verb|SubNet|s depende de la cantidad de neuronas necesarias en la capa oculta.

\begin{lstlisting}[
language={Python},
caption={},
label={alg:tpm},
%firstnumber=1,
numbers=none,
]
class Net(torch.nn.Module):
	def __init__(self, n=2, k=2):
		super(type(self), self).__init__()
		self.subnets = []
		# Creación de las SubNet's
		for i in range(k):
			self.subnets.append(SubNet(n))
			self.output = torch.nn.Linear(k, 1)
	
	def forward(self, inputs):
		output = 1
		i = 0
		for subnet in self.subnets:
			output *= subnet(inputs[i])
			i += 1
			return output
\end{lstlisting}


\subsection{Arquitectura TPM seleccionada}

Se realizaron múltiples pruebas evaluando el porcentaje de veces que dos redes se sincronizan a partir de la arquitectura seleccionada para evaluar la cantidad óptima de epochs necesarios para la sincronización. La Tabla \ref{table:config} muestra las configuraciones que lograron el mayor grado de convergencia. La mayor convergencia se logró con redes TPM de una sola neurona en la capa oculta ($k$) y 32 neuronas en la capa de entrada ($n$).

\begin{table}[htp]
  \centering
	\caption{Mejores porcentajes de convergencia obtenidos.}\label{table:config}
	\begin{tabular}{|p{0.5cm}|p{0.5cm}|p{0.5cm}|c|c|c|}
		\hline
		l &  n & k & epoch & Regla de aprendizaje & Convergencia\\
		\hline
		8 & 32 & 1  &  700 & hebb & 99.9 \\
		8 & 32 & 1  &  600 & hebb & 99.9 \\
		8 & 32 & 1  &  550 & hebb & 99.9 \\
		8 & 32 & 1  &  500 & hebb & 99.9 \\
		8 & 32 & 1  &  450 & hebb & 99.8 \\
		8 & 32 & 1  &  400 & hebb & 99.8 \\
		8 & 32 & 1  &  350 & hebb & 99.6 \\
		8 & 32 & 1  &  300 & hebb & 97.2 \\
		8 & 32 & 1  &  250 & hebb & 93.3 \\
		\hline
	\end{tabular}
\end{table}

Se seleccionó como valor óptimo para las pruebas con PyTorch la cantidad de 500 epochs ya que representa la mínima cantidad de iteraciones necesarias para lograr el mayor nivel de convergencia obtenido ($99.9 \%$).

\section{Conclusiones}

Los experimentos llevados a cabo hasta el momento dan cuenta de que el algoritmo de intercambio de claves basado en la sincronización de redes neuronales artificiales logra su objetivo con una arquitectura puntual de red TPM, y con un grado de convergencia alto para un número mínimo de iteraciones.

Se realizaron pruebas locales para determinar estos parámetros de convergencia ejecutando las tareas de actualización de pesos y comparación de salidas de dos redes TPM corriendo en una sola computadora.

Se encuentra en proceso de desarrollo y depuración la implementación de software que hace uso del algoritmo descripto en las secciones previas, y que permite la sincronización de dos redes TPM corriendo en computadoras distintas a través de una red TCP/IP. Asimismo, se encuentra en etapa de desarrollo el componente de software que permite la verificación de sincronismo mediante la combinación de funciones HASH y cifrado simétrico descripta previamente en este trabajo.

Como conclusiones preliminares del trabajo puede anticiparse que el algoritmo propuesto logra resolver el problema del intercambio de claves siméricas por medio de la sincronización de redes neuronales TPM, y sienta las bases para el desarrollo de una librería de código que pueda ser utilizada por otras aplicaciones.

Debe mencionarse también que, por las características intrínsecas del protocolo diseñado, el tiempo necesario para el intercambio de claves, y la cantiadd de mensajes intercambiados entre ambos nodos, resultan muy superiores a los requeridos por alternativas como DH o ECDH. Esto implica que las pruebas de concepto implementadas en el presente trabajo pueden dar pie a otros trabajos tendientes a optimizar el algoritmo.

%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
%\bibliographystyle{apalike}
\bibliographystyle{splncs04}
\bibliography{Crypto}

\begin{comment}
	\begin{thebibliography}{8}
\bibitem{ref_article1}
Author, F.: Article title. Journal \textbf{2}(5), 99--110 (2016)

\bibitem{ref_lncs1}
Author, F., Author, S.: Title of a proceedings paper. In: Editor,
F., Editor, S. (eds.) CONFERENCE 2016, LNCS, vol. 9999, pp. 1--13.
Springer, Heidelberg (2016). \doi{10.10007/1234567890}

\bibitem{ref_book1}
Author, F., Author, S., Author, T.: Book title. 2nd edn. Publisher,
Location (1999)

\bibitem{ref_proc1}
Author, A.-B.: Contribution title. In: 9th International Proceedings
on Proceedings, pp. 1--2. Publisher, Location (2010)

\bibitem{ref_url1}
LNCS Homepage, \url{http://www.springer.com/lncs}. Last accessed 4
Oct 2017
\end{thebibliography}

\end{comment}
\end{document}
