import csv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

results = []

with open("tests/summary.csv", "r") as f:
    # Read line
    for line in f:
        # Split line
        inner_list = [elt.strip() for elt in line.split(',')]
        results.append(inner_list)

summary_results = pd.DataFrame(results)
#print(summary_results)
summary_results.columns = ["l", "n","k","epoch","rule","convergence"]
summary_results[["l", "n","k","epoch"]] = summary_results[["l", "n","k","epoch"]].astype(int)
summary_results[["convergence"]] = summary_results[["convergence"]].astype(float)

top_value = 0
#Gráfico ordenado por epoch
sr_epoch = summary_results.copy()
sr_epoch.sort_values('epoch', ascending=True, inplace=True)
sr_epoch = sr_epoch[sr_epoch["convergence"] > top_value]
sr_epoch = sr_epoch[sr_epoch["n"] ==32]
sr_epoch = sr_epoch.dropna()
x = sr_epoch["epoch"]
y = sr_epoch["convergence"]
plt.title("Convergencia por epoch > "+str(top_value)+"%")
plt.xlabel("Epochs")
plt.ylabel("Convergencia")
plt.scatter(x, y, s=1)
plt.show()

#Gráfico ordenado por l
sr_l = summary_results.copy()
sr_l.sort_values('epoch', ascending=True, inplace=True)
sr_l = sr_l[sr_l["convergence"] > top_value]
sr_l = sr_epoch[sr_l["n"] ==32]
sr_l = sr_l.dropna()
x = sr_l["l"]
y = sr_l["convergence"]
plt.title("Convergencia por l > "+str(top_value)+"%")
plt.xlabel("l")
plt.ylabel("Convergencia")
plt.scatter(x, y, s=1)
plt.show()

#Gráfico ordenado por regla
sr_rule = summary_results.copy()
sr_rule.sort_values('rule', ascending=True, inplace=True)
sr_rule = sr_rule[sr_rule["convergence"] > top_value]
sr_rule= sr_rule[sr_rule["n"] ==32]
sr_rule = sr_rule.dropna()
x = sr_rule["rule"]
y = sr_rule["convergence"]
plt.title("Convergencia por regla de aprendizaje > "+str(top_value)+"%")
plt.xlabel("Regla de aprendizaje")
plt.ylabel("Convergencia")
plt.scatter(x, y, s=1)
plt.show()

#Gráfico ordenado por k
sr_k = summary_results.copy()
sr_k.sort_values('k', ascending=True, inplace=True)
sr_k = sr_k[sr_k["convergence"] > top_value]
sr_k= sr_k[sr_k["n"] ==32]
sr_k = sr_k.dropna()
x = sr_k["k"]
y = sr_k["convergence"]
plt.title("Convergencia por k > "+str(top_value)+"%")
plt.xlabel("K")
plt.ylabel("Convergencia")
plt.scatter(x, y, s=1)
plt.show()


#Gráfico ordenado por n
sr_n = summary_results.copy()
sr_n.sort_values('n', ascending=True, inplace=True)
sr_n = sr_n[sr_n["convergence"] > top_value]
sr_n= sr_n[sr_n["n"] ==32]
sr_n = sr_n.dropna()
x = sr_n["n"]
y = sr_n["convergence"]
plt.title("Convergencia por n > "+str(top_value)+"%")
plt.xlabel("N")
plt.ylabel("Convergencia")
plt.scatter(x, y, s=1)
plt.show()

print("Resultados filtrados")
results = summary_results.copy()
results.sort_values('convergence', ascending=False, inplace=True)
results = results[results["convergence"] > 80]
results = results[results["k"] == 1]
results = results[results["n"] >= 32]
#results = results[results["epoch"] < 600]
results = results.dropna()
print(results)
